<?php

//ceci remplace l'instruction quand on défini un namespace à la classe
use PHPUnit\Framework\TestCase;

include_once(__DIR__."/../models/Seance.php");
include_once(__DIR__."/../models/User.php");
include_once(__DIR__."/../models/Database.php");

final class SeanceTest extends TestCase{
    public function testCreateSeance(){
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");

        $database = new Database();

        $this->assertNotFalse($database->createSeance($seance));
    }

    public function testGetSeanceById(){
        $database = new Database();
        //Je crée et j'insèrt une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        //Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        //je certifie que la séance existe
        $this->assertInstanceOf(Seance::class, $database->getSeanceById($id));
    }

    public function testGetSeanceByWeek(){
        $database = new Database();
        //Je crée et j'insèrt une séance à la date d'aujourd'hui
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        //j'insère la séance en vérifiant que ça c'est bien passé
        $this->assertNotFalse($database->createSeance($seance));
        //Je compte le nombre de séances en lui passant le numéro de al semaine courante
        $nbSeances = count($database->getSeanceByWeek(date("W")));
        echo($nbSeances);
        //Et je vérifie qu'il y a au moins une séeance dans la BD
        $this->assertGreaterThan(0, $nbSeances);
    }

    ///////// et on ajoute la fonction tearDownAfterClass dans notre classe de test:// page 45 2ème/////////
    public static function tearDownAfterClass(){
        $database = new Database();
        $database->deleteAllInscrit();
        $database->deleteAllUser();
        $database->deleteAllSeance();   
    }

    ////////////////////Test de la mise à jour de la DB - à écrir dans TestSeance page 46 en-bas//////////////////
    public function testUpdateSeance(){
        $database = new Database();
        //Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        //Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        //je récupère la séance en base de données
        $seance = $database->getSeanceById($id);
        $this->assertInstanceOf(Seance::class, $seance);
        // Je modifie la séance
        $seance->setTitre("Yoga");
        // Je vérifie que la séance est mise à jour
        $this->assertTrue($database->updateSeance($seance));
    }

    ///////////// Test de la requêtes SQL de la suppression d'une séance p47 en-bas//////////////
    public function testDeleteSeance(){
        $database = new Database();
        //Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        //Je récupère l'id de la séance
        $id = $database->createSeance($seance);
        //je supprime la séance et vérifie le résultat
        $this->assertTrue($database->deleteSeance($id));
    }

    /*//////// Test d'inscription et desinscription d'un utilisateur ////////////
    ///////////////à une séance - a rajouter dans la classe SeanceTest p53 - Milieu //////*/
    public function testInsertDeleteParticipant(){
        $database = new Database();
        //Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
        // Je le sauvegarde en BD et vérifie que tout s'est bien passé
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        //Je crée une séance
        $seance = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance = $database->createSeance($seance);
        $this->assertNotFalse($idSeance);
        // J'insère un participant
        $this->assertTrue($database->insertParticipant($idSeance, $idUser));
        // Je supprime le participant
        $this->assertTrue($database->deleteParticipant($idSeance, $idUser));
    } 

     /* /////// Pour TESTER la Fonction qui permet de retrouver  toutes les séances auxquelles est 
    insctrit le user - a rajouter dans la classe SeanceTest page 59 ////// */
    
    public function testGetSeanceByUserId(){
        $database = new Database();
        // Je crée un user
        $user = User::createUser("Toto", "toto@gmail.com", password_hash("1234", PASSWORD_DEFAULT),
                                    0, 0, bin2hex(random_bytes(20)));
        // Je le sauvegarde en BD et vérifie que tout s'est bien passé
        $idUser = $database->createUser($user);
        $this->assertNotFalse($idUser);
        // Je vérifie que je récupère un tableau vide de séance pour ce user
        $this->assertEquals(0, count($database->getSeanceByUserId($idUser)));
        // Je crée une séance
        $seance1 = Seance::createSeance("Pilates", "Ce cours détend", "09:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance1 = $database->createSeance($seance1);
        $this->assertNotFalse($idSeance1);
        // J'inscrit l'utilisateur à cette séance
        $this->assertTrue($database->insertParticipant($idSeance1, $idUser));
        // Je vérifie que je récupère un tableau avec une séance pour ce user
        $this->assertEquals(1, count($database->getSeanceByUserId($idUser)));
        // Je crée une deuxième séance
        $seance2 = Seance::createSeance("Yoga", "Ce cours détend", "10:00", date("Y-m-d"), 50, 20, "#03bafc");
        // Je récupère l'id de la séance
        $idSeance2 = $database->createSeance($seance2);
        $this->assertNotFalse($idSeance2);
        // J'inscrit l'utilisateur à cette séance
        $this->assertTrue($database->insertParticipant($idSeance2, $idUser));
        // Je vérifie que je récupère un tableau avec deux séances pour ce user
        $this->assertEquals(2, count($database->getSeanceByUserId($idUser)));
    }





}
