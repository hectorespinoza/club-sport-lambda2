<?php
    // Ce fichier sert à inscrire un user à une séance

    // On va utiliser les sessions pour passer des messages d'une page à l'autre
    // Pour cela il faut démarrer la session au début des pages concernées
    session_start();

    require_once(__DIR__ ."/../models/Database.php");
    $database = new Database();

    // Récupérer l'id de la séance dans l'url
    $idSeance = $_GET["id"];

    // Récupérer l'id du user dans la session
    $idUser = $_SESSION["id"];

    // Effectuer l'inscription en base de données
    if($database->insertParticipant($idSeance, $idUser)){
        // Si ça c'est bien passé
        $_SESSION["info"] = "Vous avez bien été inscrit à cette séance";
    }else{
        // Si ça c'est mal passé
        $_SESSION["error"] = "Nous n'avons pas réussi à vous inscrire à cette séance";
    }
    header("location: ../vues/cours.php?id=".$idSeance);
    exit();